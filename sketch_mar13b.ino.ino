#include <Servo.h>

#include <Adafruit_LiquidCrystal.h>

#define trigPin 2
#define echoPin 3

#define buzzer 12
#define rPin 6 // Red pin of RGB LED
#define gPin 7 // Green pin of RGB LED 
long duration;
int distance;
Adafruit_LiquidCrystal lcd_1(0);
// Pin definitions
const int buttonPin1 = 5;
const int buttonPin2 = 4;
const int servoPin = 9;
const int redPin = 10;
const int greenPin = 11;
const int bluePin = 12;

Servo myServo;
bool servoState = false;

void setup() {
  // Initialize pins
  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  // Attach the servo motor to the servo pin
  myServo.attach(servoPin);

  // Set initial LED state to off
  setRGBColor(0, 0, 0);

  // Initialize serial communication at 9600 baud
  Serial.begin(9600);
  
    pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(buzzer, OUTPUT);

  pinMode(rPin, OUTPUT);
  pinMode(gPin, OUTPUT);
 

  lcd_1.begin(16, 2);
  lcd_1.print("Detecting Machine :");
}

void loop() {

  
  
    // Clear the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Read the echoPin
  duration = pulseIn(echoPin, HIGH);

  // Convert duration to distance in cm
  distance = duration * 0.034 / 2;

  // Display the distance on the LCD
  lcd_1.setCursor(0, 1);
  lcd_1.print(distance);
  lcd_1.print(" cm - Andrea");

  // Check if distance is valid (avoid negative or zero distances)
  if (distance <= 2) {
    // Object detected within 2 cm
    tone(buzzer, 2000); // Increase the frequency to 2000 Hz for louder sound
    analogWrite(rPin, 0);  // Turn off the red LED
    analogWrite(gPin, 255); // Turn on the green LED
    
  } else {
    // No object or object beyond 2 cm
    noTone(buzzer); // Turn off the buzzer
    analogWrite(rPin, 255);  // Turn on the red LED
    analogWrite(gPin, 0);    // Turn off the green LED
    
  }
  
    // Read the state of the push buttons
  bool button1State = digitalRead(buttonPin1);
  bool button2State = digitalRead(buttonPin2);

  // If button 1 is pressed, set servoState to true and set RGB LED to green
  if (button1State == HIGH) {
  
    servoState = true;
    setRGBColor(0, 255, 0); // Green
    Serial.println("Servo motor is ON");
  }

  // If button 2 is pressed, set servoState to false, stop the servo and set RGB LED to red
  if (button2State == HIGH) {
    
    servoState = false;
    myServo.write(0); // Move servo to 0 degrees
    setRGBColor(255, 0, 0); // Red
    Serial.println("Servo motor is OFF");
  }

  // If the servo is on, move it continuously
  if (servoState) {
    myServo.write(90); // Move servo to 90 degrees
    delay(1000); // Wait for a second
    Serial.println("Servo position: 90 degrees");

    myServo.write(0); // Move servo to 0 degrees
    delay(1000); // Wait for a second
    Serial.println("Servo position: 0 degrees");
  }

  delay(50);

}

// Function to set the RGB LED color
void setRGBColor(int red, int green, int blue) {
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);
}

